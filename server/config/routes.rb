Server::Application.routes.draw do
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)
  match 'login' => 'access#login', :as => :login
  match 'logout' => 'access#logout', :as => :logout
  match 'callback' => 'access#callback', :as => :callback

  match 'info' => 'info#index', :as => :info, :via => :get
  #match 'info/:id' => 'info#show', :via => :get
  match 'info/:id' => 'info#update', :via => :put

  match 'skill_tree/learn' => 'skill_tree#learn', :via => :put
  match 'skill_tree/query/:uid/:sid' => 'skill_tree#query', :via => :get

  match 'store' => 'store#update', :via => :put
  match 'store' => 'store#index', :via => :get
  match 'store/:id' => 'store#show', :via => :get
  match 'store/:id/sell' => 'store#sell', :via => :put
  match 'store/:id/buy' => 'store#buy', :via => :put

  match 'monster' => 'monster#index', :via => :get
  match 'monster' => 'monster#create', :via => :put
  match 'monster/:id/hit' => 'monster#hit', :via => :put

  match 'map' => 'main#map', :as => :map
  match 'item' => 'item#index', :as => :item

  match 'attack/:id' => 'battle#attack', :via => :put

  match 'chat' => 'chat#new', :via => :put

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'main#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
