# http://www.movable-type.co.uk/scripts/latlong.html
include Math
module SphereGeo
  R = 6378137.0
  # not meant to be public, but just lazy
  def SphereGeo.sq(x)
    x * x
  end
  def SphereGeo.DistanceBetween(lat1, lng1, lat2, lng2)
    lat1 *= PI / 180.0
    lat2 *= PI / 180.0
    lng1 *= PI / 180.0
    lng2 *= PI / 180.0
    R * 2 * asin(sqrt( sq(sin(0.5*(lat1-lat2))) + cos(lat1) * cos(lat2) * sq(sin(0.5*(lng1-lng2)))))
  end
  def SphereGeo.Heading(lat1, lng1, lat2, lng2)
    lat1 *= PI / 180.0
    lat2 *= PI / 180.0
    lng1 *= PI / 180.0
    lng2 *= PI / 180.0
    y = sin(lng2-lng1)*cos(lat2)
    x = cos(lat1)*sin(lat2)-sin(lat1)*cos(lat2)*cos(lng2-lng1) 
    if y.abs < 1e-8 && x.abs < 1e-8
      return 0
    else
      return atan2(y, x) / PI * 180.0
    end
  end
  def SphereGeo.Offset(lat1, lng1, th, d)
    lat1 *= PI / 180.0
    lng1 *= PI / 180.0
    th *= PI / 180.0
    lat2 = sin(lat1) * cos(d/R) + cos(lat1) * sin(d/R) * cos(th)
    y = sin(th) * sin(d/R) * cos(lat1)
    x = cos(d/R) - sin(lat1) * lat2
    if y.abs < 1e-8 && x.abs < 1e-8
      lng2 = lng1
    else
      lng2 = lng1 + atan2(y, x)
    end
    lat2 = asin(lat2)
    [lat2 * 180.0 / PI, lng2 * 180.0 / PI]
  end
end
