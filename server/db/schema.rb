# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120117015344) do

  create_table "items", :force => true do |t|
    t.string   "name"
    t.string   "pic_url"
    t.string   "description"
    t.string   "item_type"
    t.text     "effect"
    t.text     "requirement"
    t.integer  "price"
    t.integer  "user_id"
    t.integer  "store_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "monsters", :force => true do |t|
    t.string   "name"
    t.float    "max_hp"
    t.float    "max_mp"
    t.float    "hp"
    t.float    "mp"
    t.float    "speed"
    t.decimal  "longitude"
    t.decimal  "latitude"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "owns", :force => true do |t|
    t.integer  "count"
    t.integer  "user_id"
    t.integer  "store_id"
    t.integer  "item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "skill_trees", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "ancestry"
    t.string   "name"
    t.boolean  "learnt"
    t.integer  "user_id"
    t.decimal  "x"
    t.decimal  "y"
    t.boolean  "equipped"
  end

  add_index "skill_trees", ["ancestry"], :name => "index_skill_trees_on_ancestry"

  create_table "stores", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "pic_url"
    t.decimal  "longitude",   :precision => 10, :scale => 0
    t.decimal  "latitude",    :precision => 10, :scale => 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "storetype"
    t.string   "storeid"
  end

  create_table "users", :force => true do |t|
    t.string   "fid"
    t.string   "name"
    t.float    "max_hp"
    t.float    "max_mp"
    t.float    "hp"
    t.float    "mp"
    t.integer  "exp"
    t.integer  "money"
    t.decimal  "longitude",        :precision => 10, :scale => 0
    t.decimal  "latitude",         :precision => 10, :scale => 0
    t.text     "element_exp"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "player_img"
    t.datetime "last_online_time"
    t.decimal  "next_lat",         :precision => 10, :scale => 0
    t.decimal  "next_lng",         :precision => 10, :scale => 0
    t.float    "speed"
    t.datetime "last_pos_update"
    t.text     "chatlog"
    t.integer  "updated"
  end

end
