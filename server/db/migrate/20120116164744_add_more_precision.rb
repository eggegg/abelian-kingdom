class AddMorePrecision < ActiveRecord::Migration
  def change
    change_column :users, :longitude, :decimal, :precision => 17, :scale => 14
    change_column :users, :latitude, :decimal, :precision => 17, :scale => 14
  end
end
