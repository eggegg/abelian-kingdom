class ChangeHpmpToFloat < ActiveRecord::Migration
  def up
    change_column :users, :hp, :float
    change_column :users, :mp, :float
    change_column :users, :max_hp, :float
    change_column :users, :max_mp, :float
  end

  def down
    change_column :users, :hp, :integer
    change_column :users, :mp, :integer
    change_column :users, :max_hp, :integer
    change_column :users, :max_mp, :integer
  end
end
