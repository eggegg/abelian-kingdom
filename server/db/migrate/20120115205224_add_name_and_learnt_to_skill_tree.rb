class AddNameAndLearntToSkillTree < ActiveRecord::Migration
  def change
    add_column :skill_trees, :name, :string
    add_column :skill_trees, :learnt, :boolean
  end
end
