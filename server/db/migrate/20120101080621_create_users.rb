class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :fid, :unique => true, :key => true
      t.string :name, :unique => true
      t.integer :max_hp
      t.integer :max_mp
      t.integer :hp
      t.integer :mp
      t.integer :exp
      t.integer :money
      t.decimal :longitude
      t.decimal :latitude
      t.text :element_exp
      t.text :skill_tree
      t.timestamps
    end
  end
end
