class AddAncestryToSkillTree < ActiveRecord::Migration
  def change
    add_column :skill_trees, :ancestry, :string
    add_index :skill_trees, :ancestry
  end
end
