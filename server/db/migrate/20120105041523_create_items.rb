class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.string :pic_url
      t.string :description
      t.string :item_type
      t.text :effect
      t.text :requirement
      t.integer :price

      t.references :user
      t.references :store
      t.timestamps
    end
  end
end
