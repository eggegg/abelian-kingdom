class AddLocationTimeToUser < ActiveRecord::Migration
  def change
    add_column :users, :last_pos_update, :timestamp
  end
end
