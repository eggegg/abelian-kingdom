class AddChatLogToUser < ActiveRecord::Migration
  def change
    add_column :users, :chatlog, :text
  end
end
