class AddUpdatedToUser < ActiveRecord::Migration
  def change
    add_column :users, :updated, :integer
  end
end
