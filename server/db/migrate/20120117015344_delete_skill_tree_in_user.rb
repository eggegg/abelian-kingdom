class DeleteSkillTreeInUser < ActiveRecord::Migration
  def change
    remove_column :users, :skill_tree
  end
end
