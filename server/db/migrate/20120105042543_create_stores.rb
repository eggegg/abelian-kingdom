class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :name
      t.string :description
      t.string :pic_url
      t.decimal :longitude
      t.decimal :latitude
      t.timestamps
    end
  end
end
