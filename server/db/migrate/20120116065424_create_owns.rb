class CreateOwns < ActiveRecord::Migration
  def change
    create_table :owns do |t|
      t.integer :count

      t.references :user
      t.references :store
      t.references :item
      t.timestamps
    end
  end
end
