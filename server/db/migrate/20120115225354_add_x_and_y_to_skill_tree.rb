class AddXAndYToSkillTree < ActiveRecord::Migration
  def change
    add_column :skill_trees, :x, :decimal
    add_column :skill_trees, :y, :decimal
  end
end
