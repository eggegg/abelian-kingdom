class AddEquippedToSkillTree < ActiveRecord::Migration
  def change
    add_column :skill_trees, :equipped, :boolean
  end
end
