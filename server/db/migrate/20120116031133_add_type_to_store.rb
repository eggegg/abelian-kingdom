class AddTypeToStore < ActiveRecord::Migration
  def change
    add_column :stores, :storetype, :string
    add_column :stores, :storeid, :string, :unique => true
  end
end
