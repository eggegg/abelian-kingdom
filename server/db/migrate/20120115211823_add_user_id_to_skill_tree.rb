class AddUserIdToSkillTree < ActiveRecord::Migration
  def change
    add_column :skill_trees, :user_id, :integer
  end
end
