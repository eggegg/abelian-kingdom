class AddTimeAndWayPointToUsers < ActiveRecord::Migration
  def change
    add_column :users, :last_online_time, :timestamp
    add_column :users, :next_lat, :decimal
    add_column :users, :next_lng, :decimal
    add_column :users, :speed, :float
  end
end
