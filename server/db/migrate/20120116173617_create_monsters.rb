class CreateMonsters < ActiveRecord::Migration
  def change
    create_table :monsters do |t|
      t.string   :name
      t.float    :max_hp
      t.float    :max_mp
      t.float    :hp
      t.float    :mp
      t.float    :speed
      t.decimal  :longitude
      t.decimal  :latitude
      t.timestamps
    end
  end
end
