class Item < ActiveRecord::Base
  has_many :owns
  def sell_price
    (self.price * 0.6).floor
  end
end
