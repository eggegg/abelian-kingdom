class SkillTree < ActiveRecord::Base
  has_ancestry
  belongs_to :user

  before_create :default_values

  # default constant
  MAXDEG = 4

  def self.randtree(owner)
    # create (randomized) skill tree of 5*3 nodes, return root
    ele = ['fire', 'water', 'thunder', 'holy', 'dark']
    rem = [3, 3, 3, 3, 3]
    available = Array.new(MAXDEG, 0)

    random_element = lambda do
      valid = 0.step(4,1).to_a.select {|i| rem[i]>0 }
      ind = valid.sample
      rem[ind] -= 1
      return ele[ind]
    end

    random_node = lambda do
      arr = 0.step(available.size-1,1).to_a
      ind = arr[0,arr.size()*0.8].sample
      ret = available[ind]
      available.delete_at(ind)
      return ret
    end

    root = SkillTree.create! :name => random_element.call, :parent => nil
    root.user = owner
    root.save
    nodelists = [root]
    for i in 1..14
      node = SkillTree.create! :name => random_element.call, :parent => nodelists[random_node.call]
      node.user = owner
      node.save
      nodelists << node
      available += Array.new(MAXDEG, i)
    end

    grow nodelists

    return root
  end

  #MAXFAIL = 50
  #ATT = 0.5
  #ITER = 30
  PI = Math.acos(-1.0)
  RAD = 65.0
  IGAP = 60.0
  def self.dfs(node,y,list)
    list[y]<<node.id
    node.children.each do |u|
      dfs(u,y+1,list)
    end
  end
  def self.contain(a,b,x)
    b+=2*PI if b<a
    x+=2*PI if x<a
    return x>=a && x<=b
  end
  def self.intersect(a,b,c,d)
    return contain(a,b,c) || contain(a,b,d) || contain(c,d,a) || contain(c,d,b)
  end
  def self.grow(nodelists)
    puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    list = []
    (0..15).each do list<<[] end
    dfs(nodelists[0],0,list)
    root = nodelists[0]
    root.x = 0
    root.y = 0
    root.save
    for i in 0..list[1].size-1
      ang = PI*0.5 + i*(2*PI)/list[1].size
      node =SkillTree.find(list[1][i])
      node.x = RAD*Math.cos(ang)
      node.y = RAD*Math.sin(ang)
      node.save
    end
    for y in 1..15
      break if list[y].size==0
      rad = RAD*(y+1)
      agap = 2*Math.asin(IGAP/(2*rad))
      wt = []
      me = []
      n = list[y].size

      ddd=[]
      eee=[]
      for i in 0..list[y+1].size-1
        ddd<<list[y+1][i]
      end
      puts "all children:" + ddd.to_s

      for i in 0..n-1
        #STDERR.puts "list[y][i]: "+list[y][i]
        puts "list[y][i]: "+list[y][i].to_s
        node = SkillTree.find(list[y][i])
        wt << node.children.size
        me << Math.atan2(node.y,node.x)
      end
      djs = DisjointSet.new(n,wt,me)
      begin
        flag = false
        for i in 0..n-1
          next if djs.rep(i)!=i
          for j in i+1..n-1
            next if djs.rep(j)!=j
            if intersect(djs.me[i]-0.5*(djs.wt[i])*agap,
                         djs.me[i]+0.5*(djs.wt[i])*agap,
                         djs.me[j]-0.5*(djs.wt[j])*agap,
                         djs.me[j]+0.5*(djs.wt[j])*agap)
              puts "NNNNN: "+i.to_s+" "+j.to_s+" "+n.to_s
              djs.merge(i,j)
              puts "QQQQQ"
              flag = true
            end
          end
        end
      end while flag
      for rep in 0..n-1
        next if djs.rep(rep)!=rep
        vis = Array.new(n,false)
        l = r = rep
        while djs.rep((l+n-1)%n)==rep && (l+1)%n!=rep do
          l=(l+n-1)%n
          puts "l: "+l.to_s+", n: "+n.to_s
        end
        while djs.rep((r+1)%n)==rep && (r+1)%n!=l do
          r=(r+1)%n
          puts "r: "+r.to_s+", n: "+n.to_s
        end
        puts "l&r: " +l.to_s + " " +r.to_s
        ch = []
        j=l;
        while true do
          puts "JJin!!.. " + j.to_s
          node = SkillTree.find(list[y][j])
          puts "node is " + node.to_s
          node.children.each do |u|
            puts "!!!!!!"
            ch << u
          end
          vis[j] = true
          break if j==r
          j=(j+1)%n
        end
        puts "group" + ch.to_s
        ws = djs.wt[rep]
        mm = djs.me[rep]
        ch.each_with_index do |u,i|
          a = mm+(i-(ws-1)*0.5)*agap
          puts "a is : "+a.to_s
          u.x = rad * Math.cos(a)
          u.y = rad * Math.sin(a)
          u.save
        end
      end
    end
    puts "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
  end

  private
  def default_values
    self.learnt = 0
    self.equipped = 0
  end
end

class DisjointSet
  def initialize(_n, _wt, _me)
    @n = _n
    @wt = _wt
    @me = _me
    @r = []
    @sz = []
    (0..@n-1).each do |i|
      @r[i] = i;
      @sz[i] = 1;
    end
  end
  def rep(v)
    puts "rep v:"+v.to_s
    @r[v]=rep(@r[v]) if @r[v]!=v
    return @r[v]
  end
  def merge(v,u)
    v=rep(v)
    u=rep(u)
    if v!=u
      @r[v]=u
      @sz[u]+=@sz[v]
      @me[u]=(@me[v]*@wt[v]+@me[u]*@wt[u]).to_f/(@wt[v]+@wt[u])
      @wt[u]+=@wt[v]
    end
  end
  def wt 
    return @wt
  end
  def me
    return @me
  end
end
