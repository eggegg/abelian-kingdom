require "SphereGeo"
class User < ActiveRecord::Base
  has_many :owns
  has_many :items, :through => :owns
  has_one :skill_tree

  validates :name, :presence => true
  validates :fid, :presence => true

  before_create :default_values

  def as_json(options)
    ret = super(options)
    ret["max_hp"] = self.get_maxhp
    ret["max_mp"] = self.get_maxmp
    ret
  end
  def get_lvl
    Math.sqrt(self.exp).floor + 1
  end
  def get_maxhp
    self.max_hp + (self.get_lvl-1) * 10
  end
  def get_maxmp
    self.max_mp + (self.get_lvl-1) * 10
  end
  def beaten
    self.hp = self.get_maxhp
    self.mp = self.get_maxmp
    self.latitude = default_lat
    self.longitude = default_lng
    self.next_lat = nil
    self.next_lng = nil
  end

  def self.for session, rest_graph
    user = User.find_by_fid(session[:fid])
    unless user
      # new user
      data = session[:me]
      user = User.new(:name => data['name'],
                      :fid  => data['id'])
      user.skill_tree = SkillTree.randtree(user)
      user.save
    end
    user
  end
  def update_all
    t = Time.now
    lt = self.last_pos_update
    self.last_pos_update = t
    dt = t.to_f - lt.to_f
    unless self.next_lat.nil? || self.next_lng.nil?
      dis = SphereGeo.DistanceBetween(self.latitude, self.longitude, self.next_lat, self.next_lng)
      heading = SphereGeo.Heading(self.latitude, self.longitude, self.next_lat, self.next_lng)
      if dt * self.speed > dis
        self.latitude = self.next_lat
        self.longitude = self.next_lng
        self.next_lat = nil
        self.next_lng = nil
      else
        self.latitude, self.longitude = SphereGeo.Offset(self.latitude, self.longitude, heading, dt * self.speed)
      end
    end
    self.hp = [self.hp+dt * 0.6, self.get_maxhp].min
    self.mp = [self.mp+dt * 0.8, self.get_maxmp].min
  end

private
  def default_lng
    121.541927754879 + (rand()-0.5) * 0.0001
  end
  def default_lat
    25.019054464486647 + (rand()-0.5) * 0.0001
  end
  def default_values
    self.max_hp     ||= 100
    self.max_mp     ||= 100
    self.hp         ||= 100
    self.mp         ||= 100
    self.exp        ||= 0
    self.money      ||= 100
    self.longitude  ||= default_lng
    self.latitude   ||= default_lat
    self.player_img ||= "player#{rand(4)+1}.png"
    self.speed      ||= 20
    self.last_pos_update ||= Time.now
    self.chatlog ||= ""
    self.updated ||= 0
  end
end
