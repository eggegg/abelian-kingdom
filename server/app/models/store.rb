class Store < ActiveRecord::Base
  has_many :owns
  has_many :items, :through => :owns
end
