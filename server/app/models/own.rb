class Own < ActiveRecord::Base
  belongs_to :user
  belongs_to :store
  belongs_to :item

  def self.add_one item
    self.add_some item,1
  end
  def self.add_some item,num
    self.add_some_by_id item.id,num
  end

  def self.add_some_by_id id,num
    if num == 0
      return
    end
    if num > 0
      own = self.find_by_item_id id

      unless own
        own = self.create(:count => num, :item => Item.find(id))
      else
        own.update_attribute(:count, own.count + num)
      end
    end
    if num < 0
      own = self.find_by_item_id id
      unless own
        return
      end
      if own.count < -num
        return
      end
      if own.count + num == 0
        self.delete own
      else
        own.update_attribute(:count, own.count + num)
      end
    end
    own
  end

  def as_json options
    ret = self.item.as_json options
    ret["count"] = self.count
    ret
  end
end
