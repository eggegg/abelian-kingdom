module SkillTreeHelper
  def display(root)
    # require:
    # rune - learnt(img1) / unlearnt(img2), draggable
    # line - connected(white) / not(grey)
    # refresh - how?
    ret = ""
    ret << "window.onload=function()"
    ret << "{"
    ret << "var c=document.getElementById('skill_tree_panel');"
    ret << "var ctx=c.getContext('2d');"
    root.subtree.each_with_index do |v,i|
      next if v.parent == nil 
      #ret << "ctx.moveTo(#{v.parent.x}, #{570-v.parent.y});"
      #ret << "ctx.lineTo(#{v.x}, #{570-v.y});"
      ret << "ctx.moveTo(#{400+v.parent.x}, #{300+v.parent.y});"
      ret << "ctx.lineTo(#{400+v.x}, #{300+v.y});"
      if v.learnt
        ret << "ctx.strokeStyle = '#AAAAAA';";
      else
        ret << "ctx.strokeStyle = '#111111';";
      end
      ret << "ctx.lineWidth = 5;";
      ret << "ctx.stroke();";
      #ret << "runex[#{i}] = #{v.x};"
      #ret << "runey[#{i}] = #{570 - v.y};"
    end
    root.subtree.each_with_index do |v,i|
      img = "rune#{i}"
      ret << "var #{img} = new Image();"
      ret << "#{img}.onload = function(){ ctx.drawImage(#{img}, #{400+v.x-17.5}, #{300+v.y-17.5}, 35, 35); };"
      ret << "#{img}.src = \"http://acm.csie.org/~kelvin/#{v.name}.png\";"
    end
    ret << "getStates()";
    ret << "}"
    javascript_tag(ret)
  end
end
