require "SphereGeo"
class BattleController < ApplicationController
  before_filter :ensure_login
  def attack
    attacker = current_user
    defender = User.find(params[:id])
    attacker.update_all
    defender.update_all
    if attacker.id != defender.id && SphereGeo.DistanceBetween(attacker.latitude, attacker.longitude, defender.latitude, defender.longitude) < 5.5
      lvl = attacker.get_lvl
      dmg = rand(20-15) + 15 + rand(lvl) + lvl
      attacker.exp += 1
      defender.hp -= dmg
      attacker.chatlog += "You attack #{defender.name} and deal #{dmg} damage.<br>"
      defender.chatlog += "#{attacker.name} attacks you and deals #{dmg} damage.<br>"
      if defender.hp <= 0
        attacker.chatlog += "You've beaten #{defender.name}.<br>"
        attacker.exp += 3
        defender.chatlog += "You've been beaten by #{attacker.name}!<br>"
        defender.beaten
      end
      attacker.save
      defender.save
    end
    render :nothing => true
  end
end
