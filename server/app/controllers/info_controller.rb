class InfoController < ApplicationController
  before_filter :ensure_login

  def index
    current_user.update_attribute(:last_online_time, Time.now)
    ou = online_user
    ou.each do |user|
      user.update_all
      user.save
    end
    current_user.update_attributes({:chatlog => "", :updated => 0})
    render :json => ou
  end
  #def show
  #  user = User.find(params[:id])
  #  render :json => user
  #end
  def update
    user = User.find(params[:id])
    user.update_all
    user.next_lat = params[:next_lat]
    user.next_lng = params[:next_lng]
    user.save
    render :nothing => true
  end
end
