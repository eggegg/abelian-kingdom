class ApplicationController < ActionController::Base
  protect_from_forgery

  rescue_from RestGraph::Error::InvalidAccessToken, :with => :ensure_login

  APP_ID = "126979764086203"
  SECRET = "d2a78705537dcae9e5644d8915f3eb0f"

protected
  def rest_graph
    @rest_graph ||= RestGraph.new(:access_token => session[:access_token],
                                  :app_id       => APP_ID,
                                  :secret       => SECRET)
    @rest_graph
  end

  def ensure_login
    redirect_to login_path unless rest_graph.authorized?
  end

  def current_user
    @current_user ||= User.for(session, rest_graph)
    @current_user
  end
  def online_user
    User.where("last_online_time >= :time",{:time => Time.now - 15})
  end
end
