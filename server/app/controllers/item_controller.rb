class ItemController < ApplicationController
  before_filter :ensure_login
  def index
    render :json => current_user.owns
  end
end
