class MainController < ApplicationController
  before_filter :ensure_login, :only => :map

  def index
  end

  def map
    @user = current_user
    render :layout => nil
  end
end
