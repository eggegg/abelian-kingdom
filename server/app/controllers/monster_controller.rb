class MonsterController < ApplicationController
  before_filter :ensure_login
  def index
    render :json => Monster.all.reject{|s| SphereGeo.DistanceBetween(s.latitude, s.longitude, current_user.latitude, current_user.longitude) >= 200}
  end
  def create
    monsters = params[:_json]
    monsters.each do |data|
      Monster.create(data)
    end
    render :nothing => true
  end
  def hit
    attacker = current_user
    monster = Monster.find(params[:id])
    attacker.update_all

    if SphereGeo.DistanceBetween(attacker.latitude, attacker.longitude, monster.latitude, monster.longitude) < 5.1
      dmg = rand(20-15) + 15
      monster_dmg = rand(20-15) + 15
      attacker.exp += 1
      monster.hp -= dmg
      attacker.chatlog += "You attack #{monster.name} and deal #{dmg} damage.<br>"
      if monster.hp <= 0
        attacker.chatlog += "You've beaten #{monster.name}.<br>"
        attacker.exp += 3
        attacker.save
        monster.destroy
        # TODO: destroy the monster for other players
        render :nothing => true
        return
      end
      attacker.hp -= monster_dmg
      attacker.chatlog += "#{monster.name} strikes back and deals #{monster_dmg} damage.<br>"
      if attacker.hp <= 0
        attacker.chatlog += "You've beaten #{defender.name}.<br>"
        attacker.chatlog += "You've been beaten by #{attacker.name}!<br>"
        attacker.beaten
      end
      attacker.save
      monster.save
      render :nothing => true
    end
  end
end
