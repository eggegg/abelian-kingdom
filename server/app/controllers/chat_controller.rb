class ChatController < ApplicationController
  def new
    logger.debug params
    ou = online_user
    name = current_user.name
    ou.each do |u|
      u.chatlog += "<span class=\"chat_user\">#{name}:</span> #{ERB::Util.html_escape(params[:text])}<br>"
      u.save
    end
    render :nothing => true
  end
end
