class StoreController < ApplicationController
  before_filter :ensure_login
  def index
    render :json => Store.all.reject{|s| SphereGeo.DistanceBetween(s.latitude, s.longitude, current_user.latitude, current_user.longitude) >= 200}
  end
  def show
    @store = Store.find(params[:id])
    @cu = current_user
    render :layout => nil
  end
  def update
    newstores = params[:_json]
    newstores.each do |data|
      if Store.find_by_storeid(data[:storeid]).nil?
        Store.create(data)
      end
    end
    render :nothing => true
  end
  def sell
    cart = params[:_json]
    flag = true
    cart.each do |data|
      id = data['id'].to_i
      num = data['num']
      own = current_user.owns.find_by_item_id(id)
      flag = false unless own && own.count >= num
    end
    if flag
      total = 0
      cart.each do |data|
        id = data['id'].to_i
        num = data['num']
        current_user.owns.add_some_by_id(id, -num)
        total += Item.find(id).sell_price * num
      end
      current_user.money += total
      current_user.chatlog += "Items sold for $#{total}.<br>"
      current_user.updated |= 1
    else
      current_user.chatlog += "You don't have enough items to sell.<br>"
    end
    current_user.save
    render :nothing => true
  end
  def buy
    cart = params[:_json]
    total = 0
    cart.each do |data|
      id = data['id'].to_i
      num = data['num']
      total += Item.find(id).price * num
    end
    if total <= current_user.money
      current_user.money -= total
      cart.each do |data|
        id = data['id'].to_i
        num = data['num']
        current_user.owns.add_some_by_id(id, num)
      end
      current_user.chatlog += "Items bought. $#{total} spent.<br>"
      current_user.updated |= 1
    else
      current_user.chatlog += "You don't have enough money. (Total: $#{total})<br>"
    end
    current_user.save
    render :nothing => true
  end
end
