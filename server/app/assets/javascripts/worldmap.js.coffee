placesService = {}
class window.LatLngControl extends gmaps.OverlayView
    constructor : (map) ->
        @setMap(map)
        this.set('visible', false)
    draw : () ->

    getPosition : (latLng) ->
        projection = @getProjection()
        projection.fromLatLngToContainerPixel(latLng)
class window.WorldMap
    initialize : (location) ->
        myOptions = {
            zoom: 20,
            center: location,
            mapTypeId: gmaps.MapTypeId.HYBRID,
            disableDefaultUI: true,
            disableDoubleClickZoom: true,
            draggable: false,
            scrollwheel: false,
            styles: [{
                featureType: "poi",
                elementType: "labels",
                stylers: [{
                    visibility: "off"
                }]
            }],
            noClear: true
        }
        @map = new gmaps.Map($('#map_canvas')[0], myOptions)
        window.latlngcontrol = new LatLngControl(@map)
        placesService = new gmaps.places.PlacesService(@map)
        ###
        window.infowindow = new gmaps.InfoWindow({
            disableAutoPan: true
        })
        ###

        @setCenter(location)
        gmaps.event.addListener(@map, 'click', (event) ->
            if window.subwindow?
                window.subwindow.close()
            else
                worldmap.myplayer.moveto(event.latLng)
        )
        @places = new gmaps.MVCArray()
    setCenter : (location) ->
       @map.setCenter(location)
       if not @lastPlaceLocation? || gmaps.geometry.spherical.computeDistanceBetween(@lastPlaceLocation, location) > 50
          placesService.search({
             location: location,
             radius: '100',
             # http://code.google.com/intl/en/apis/maps/documentation/places/supported_types.html
             # types: ['bus_station']
             types: ['bank', 'book_store', 'bus_station', 'cafe', 'car_dealer', 'casino', 'church', 'clothing_store', 'convenience_store', 'department_store', 'food', 'hospital', 'library', 'museum', 'park', 'pet_store', 'post_office', 'restaurant', 'school', 'store', 'train_station', 'university', 'movie_theater']
          },
          (result, status) =>
             if status == gmaps.places.PlacesServiceStatus.OK
                ret = []
                for place in result
                    obj = {
                       latitude: place.geometry.location.lat(),
                       longitude: place.geometry.location.lng(),
                       name: place.name,
                       pic_url: place.icon,
                       storetype: place.types.join(' '),
                       storeid: place.id
                    }
                    ret.push(obj)
                $.put('store',JSON.stringify(ret),$.noop,'json')
             else
                console.log(status)
          )
          window.storelist.update() if window.storelist?
          @lastPlaceLocation = location

