window.GameView = Backbone.View.extend({
    initialize : () ->
        window.controllerlist = {}
        window.storecontrollerlist = {}
        window.itemcontrollerlist = {}

        window.playerlist.update()
        window.storelist.update()
        window.myitemlist.update()
        #@updatetimer = 0
        @timer = () =>
            if @onTick() && not @stopTimer?
                setTimeout(@timer ,1000/window.fps)
        @timer()
        @updateplayertimer = 0
    onTick : () ->
        window.playerlist.each((player) ->
            player.controller.onTick()
        )
        @updateplayertimer++
        if @updateplayertimer * window.ft >= 200
            window.playerlist.update(false)
            @updateplayertimer = 0
        return true
})
