roadWidth = 5
directionService = new gmaps.DirectionsService()
window.PlayerController = Backbone.View.extend({
    initialize: () ->
        @model.controller = this
        @marker = new gmaps.Marker({
            position: @model.getLatLng(),
            map: window.worldmap.map
        })
        
        @spriteWidth = 32
        @spriteHeight = 48
        @markericon = new gmaps.MarkerImage(
            "assets/#{@model.get('player_img')}",
            new gmaps.Size(@spriteWidth, @spriteHeight),
            new gmaps.Point(0, 0)
        )
        @marker.setIcon(@markericon)
        @frameID = 0
        @direction = 0
        @animatefps = 4
        @animateft = 1000.0 / @animatefps
        if @model.id == window.userID
            @isme = true
            window.worldmap.myplayer = this
            window.worldmap.setCenter(@model.getLatLng())
        else
            @isme = false
        gmaps.event.addListener(@marker, 'click', () =>
            worldmap.myplayer.moveto(@model.getLatLng(), () =>
                $.put("attack/#{@model.id}",$.noop)
            ,5)
        ) unless @isme
        if @isme
            @updateBar("hp", @model.get('hp'), @model.get('max_hp'), 'HP ')
            @updateBar("mp", @model.get('mp'), @model.get('max_mp'), 'MP ')
            @updateExp()
            @updateMoney()
        else
            @el = $(JST['backbone/templates/smallbar']({id: @model.id, name: @model.get('name')}))
            @el.appendTo('body')
            @updateBar("hp#{@model.id}", @model.get('hp'), @model.get('max_hp'))
            @updateBar("mp#{@model.id}", @model.get('mp'), @model.get('max_mp'))
            @updateBarPosition()
        @model.bind('change', @onModelChange, this)
    updateBar : (item, val, maxval, text) ->
        $("div##{item}_now").css('width', "#{val * 100.0 / maxval}%")
        $("span##{item}_text").text("#{text}#{Math.floor(val)}/#{Math.floor(maxval)}") if text?
    updateExp : () ->
        # The most simple level system
        oldexp = @model.previous('exp')
        oldlvl = @model.getLevel(oldexp)
        exp = @model.get('exp')
        lvl = @model.getLevel(exp)
        if lvl > oldlvl
            window.addLog(JST['backbone/templates/levelup']({lvl: lvl}))
        else if lvl < oldlvl
            window.addLog(JST['backbone/templates/leveldown']({lvl: lvl}))
        cur = exp - (lvl - 1) * (lvl - 1)
        max = 2 * lvl - 1
        @updateBar("exp", cur, max, "EXP ")
        $('div#player_lv').text("Lv#{lvl}")
    updateMoney : () ->
        money = @model.get('money').toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
        $("div#money").text("$#{money}")
    updateBarPosition : () ->
        pos = window.latlngcontrol.getPosition(@model.getLatLng())
        pos.y -= @spriteHeight
        pos.y -= @el.height()
        pos.x -= @el.width()/2
        pos.y -= 10
        @el.css('top',pos.y).css('left', pos.x)
    onModelChange : () ->
        if @model.hasChanged('latitude') || @model.hasChanged('longitude')
            @marker.setPosition(@model.getLatLng())
            if @isme
                window.worldmap.setCenter(@model.getLatLng())
                @poly.getPath().setAt(0, @model.getLatLng()) if @poly?
                for id of window.controllerlist
                    window.controllerlist[id].updateBarPosition() unless window.controllerlist[id].isme
            else
                @updateBarPosition()
        if @isme
            @updateBar("hp", @model.get('hp'), @model.get('max_hp'), 'HP ') if @model.hasChanged('max_hp') || @model.hasChanged('hp')
            @updateBar("mp", @model.get('mp'), @model.get('max_mp'), 'MP ') if @model.hasChanged('max_mp') || @model.hasChanged('mp')
        else
            @updateBar("hp#{@model.id}", @model.get('hp'), @model.get('max_hp')) if @model.hasChanged('max_hp') || @model.hasChanged('hp')
            @updateBar("mp#{@model.id}", @model.get('mp'), @model.get('max_mp')) if @model.hasChanged('max_mp') || @model.hasChanged('mp')
        return unless @isme
        if @model.hasChanged('updated')
            window.myitemlist.update() if (@model.get('updated') & 1) != 0 # item updated
        @model.mysave() if @model.hasChanged('next_lat') || @model.hasChanged('next_lon')
        @updateExp() if @model.hasChanged('exp')
        @updateMoney() if @model.hasChanged('money')
        if @model.hasChanged('chatlog') && @isme
            window.addLog(@model.get('chatlog'))
            @model.set({chatlog: ""})
    onTick : () ->
        loc = @model.getLatLng()
        if @isme && @poly?
            path = @poly.getPath()
            if path.getLength() > 1 && gmaps.geometry.spherical.computeDistanceBetween(loc, path.getAt(1)) < 0.1
                path.removeAt(0)
            if path.getLength() == 1
                @poly.setMap(null)
                @poly = null
                @model.set({
                    next_lat: null,
                    next_lng: null
                })
            else
                @model.set({
                    next_lat: path.getAt(1).lat(),
                    next_lng: path.getAt(1).lng()
                })
        loc2 = @model.getNextLatLng()
        if loc2?
            heading = gmaps.geometry.spherical.computeHeading(loc, loc2)
            if gmaps.geometry.spherical.computeDistanceBetween(loc, loc2) < @model.get('speed') * window.ft / 1000.0
                nextloc = loc2
            else
                nextloc = gmaps.geometry.spherical.computeOffset(loc, @model.get('speed') * window.ft / 1000.0, heading)
                if heading <= 35 && heading > -35
                    @direction = 3
                else if heading > 35 && heading <= 145
                    @direction = 2
                else if heading <= -35 && heading > -145
                    @direction = 1
                else
                    @direction = 0
            @model.set({
                latitude: nextloc.lat(),
                longitude: nextloc.lng()
            })
        @frameID += window.ft
        @frameID = 0 if @frameID >= 4 * @animateft
        @markericon.origin.x = Math.floor(@frameID / @animateft) * @spriteWidth
        @markericon.origin.y = @direction * @spriteHeight
        @marker.setIcon(@markericon)
    clear : () ->
        @marker.setMap(null)
        @marker = null
        if @poly?
            @poly.setMap(null)
            @poly = null
        unless @isme
            @el.remove()
        window.playerlist.remove(@model)
    moveto : (location, callback, dist) -> # callback when route = 0
        if dist? && gmaps.geometry.spherical.computeDistanceBetween(@model.getLatLng(), location) <= dist
            callback()
            return
        request = {
            origin: @model.getLatLng(),
            destination: location,
            travelMode: gmaps.TravelMode.WALKING
        }
        directionService.route(request, (result, status) =>
            if status == gmaps.DirectionsStatus.OK
                points = new gmaps.MVCArray(result.routes[0].overview_path)
                points.setAt(0, @model.getLatLng())
                endpoint = points.getAt(points.getLength() - 1)
                dis = gmaps.geometry.spherical.computeDistanceBetween(endpoint, location)
                if dis > roadWidth
                    heading = gmaps.geometry.spherical.computeHeading(endpoint, location)
                    location = gmaps.geometry.spherical.computeOffset(endpoint, roadWidth, heading)
                points.setAt(points.getLength() - 1, location)
                if dist?
                    p1 = points.getAt(points.getLength() - 1)
                    p2 = points.getAt(points.getLength() - 2)
                    d2 = gmaps.geometry.spherical.computeDistanceBetween(p1, p2)
                    if d2 <= dist
                        points.removeAt(points.getLength() - 1)
                    else
                        heading = gmaps.geometry.spherical.computeHeading(p1, p2)
                        location = gmaps.geometry.spherical.computeOffset(p1, dist, heading)
                        points.setAt(points.getLength() - 1, location)
                if @poly?
                    @poly.setMap(null)
                    @poly = null
                @poly = new gmaps.Polyline({
                    map: worldmap.map,
                    path: points,
                    strokeColor: "#FF0000",
                    strokeOpacity: 0.5,
                    strokeWeight: 4
                })
                if callback? && gmaps.geometry.spherical.computeDistanceBetween(points.getAt(0), points.getAt(points.getLength()-1)) <= 0.1
                    callback()
            else
                alert(status)
        )
})
window.Player = Backbone.Model.extend({
    getLatLng : () ->
        return new gmaps.LatLng(parseFloat(@get('latitude')), parseFloat(@get('longitude')))
    getNextLatLng : () ->
        if @has('next_lat')
            return new gmaps.LatLng(parseFloat(@get('next_lat')), parseFloat(@get('next_lng')))
        else
            return null
    getLevel : (exp) ->
        exp = @get('exp') unless exp?
        Math.floor(Math.sqrt(exp)) + 1
    mysave : () ->
        obj = {}
        for it in ['next_lat', 'next_lng', 'id']
            obj[it] = @get(it) if @has(it)
        $.put("info/#{@id}", JSON.stringify(obj), $.noop, 'json')
})
window.PlayerList = Backbone.Collection.extend({
    model: Player
    #url: '/info' # not used
    update: (updatepos) ->
        $.get('info', (ret) ->
            newed = {}
            for item in ret
                id = item['id']
                newed[id] = true
                if window.controllerlist[id]?
                    if window.controllerlist[id].isme
                        r = (item['chatlog'].indexOf("been beaten") != -1)
                        unless r
                            if !updatepos
                                delete item['latitude']
                                delete item['longitude']
                            delete item['next_lat']
                            delete item['next_lng']
                    window.controllerlist[id].model.set(item)
                else
                    player = new Player(item)
                    window.playerlist.add(player)
                    window.controllerlist[id] = new PlayerController({
                        model: player
                    })
                    window.addLog("#{item['name']} is now online.<br>")
            for id of window.controllerlist
                unless newed[id]
                    window.addLog("#{window.controllerlist[id].model.get('name')} has left.<br>")
                    window.controllerlist[id].clear()
                    delete window.controllerlist[id]
        )
})
