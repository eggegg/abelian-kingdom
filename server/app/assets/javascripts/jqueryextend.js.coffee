# Extend jQuery with functions for PUT and DELETE requests.

_ajax_request = (url, data, callback, type, method) ->
    if $.isFunction(data)
        callback = data
        data = ""
    return $.ajax({
        type: method,
        url: url,
        data: data,
        contentType: 'application/json',
        success: callback,
        dataType: type,
        processData: false
    })

$.extend({
    put: (url, data, callback, type) ->
        return _ajax_request(url, data, callback, type, 'PUT')
    ,delete_: (url, data, callback, type) ->
        return _ajax_request(url, data, callback, type, 'DELETE')
})
