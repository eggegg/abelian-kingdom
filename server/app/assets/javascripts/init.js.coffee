window.gmaps = google.maps
window.defaultLocation = new gmaps.LatLng(25.01657, 121.53303)
window.fps = 20
window.ft = 1000.0 / window.fps

$(() ->
    # Backbone instantiate
    window.worldmap = new WorldMap()
    window.worldmap.initialize(defaultLocation)

    window.playerlist = new PlayerList()
    window.storelist = new StoreList()
    window.myitemlist = new ItemList()

    window.gameview = new GameView()

    # Toggle panels
    $('#chat_expand').toggle (() ->
        $('#chat_history').hide('slow')
        $('#chat_expand').html('&#9654;')), (() ->
        chat = $('#chat_history')
        chat.show('slow')
        chat.scrollTop(chat[0].scrollHeight)
        $('#chat_expand').html('&#9650;')
    )
    $('#right_expand').toggle (() ->
        $('#right_panel_lower').hide('slow')
        $('#right_expand').html('&#9654;')), (() ->
        $('#right_panel_lower').show('slow')
        $('#right_expand').html('&#9660;')
    )

    # Toggle skilltree
    $('#open_skill_tree').toggle  (() ->
        $('#skill_tree_panel').show('slow')
        $('#open_skill_tree').html('&#8853;')), (() ->
        $('#skill_tree_panel').hide('slow')
        $('#open_skill_tree').html('&#8853;')
    )

    # Modal view
    window.modalShow = (id) ->
        maskHeight = $(document).height()
        maskWidth = $(window).width()
        modelMask = $('#modal_mask')

        modelMask.css({'width':maskWidth, 'height':maskHeight}).fadeTo("fast", 0.6)

        winH = $(window).height()
        winW = $(window).width()

        $(id).css('top',  winH/2-$(id).height()/2).css('left', winW/2-$(id).width()/2).fadeIn(1000)
    window.modalHide = (e) ->
        e.preventDefault()
        $('#modal_mask').fadeOut(500)
        $('.modal_window').fadeOut(500)
    $('#modal_mask').click(() ->
        $(this).fadeOut(500)
        $('.modal_window').fadeOut(500)
    )

    # Right tab
    $('#inventory').click(() ->
        $('#inventory_panel').show()
        $('#equipment_panel').hide()
        $('#inventory').css({'border-bottom': '3px solid #33b5e5'})
        $('#equipment').css({'border-bottom': '0px'})
    )
    $('#equipment').click(() ->
        $('#inventory_panel').hide()
        $('#equipment_panel').show()
        $('#equipment').css({'border-bottom': '3px solid #33b5e5'})
        $('#inventory').css({'border-bottom': '0px'})
    )

    # Chat listener
    $('button#chat_button').click((event) -> 
        event.preventDefault()
        text = $('input#chat_input').val().trim()
        $.put('chat',JSON.stringify({text: text}),$.noop,'json') unless text.length == 0
        $('input#chat_input').val("").focus()
    )
    $('input#chat_input').keypress((e) ->
        $('button#chat_button').focus().click() if e.which == 13 # Enter key
    )
)
