# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
window.StoreController = Backbone.View.extend({
    initialize : () ->
        placeLoc = @model.getLatLng()
        image = new gmaps.MarkerImage(@model.get('pic_url'), undefined, undefined, undefined, new gmaps.Size(32, 32))
        @marker = new gmaps.Marker({
            map: window.worldmap.map,
            position: placeLoc,
            icon: image
        })
        gmaps.event.addListener(@marker, 'click', () =>
            worldmap.myplayer.moveto(@model.getLatLng(), () =>
                $.get("store/#{@model.id}", (data) =>
                    $('div#store').html(data)
                    @initStoreUI()
                    window.modalShow('div#store')
                )
            )
        )
    initStoreUI : () ->
        $('#store_buy').css({'border-bottom': '3px solid #33b5e5'})
        @storepage = "buy"
        $('#store_sell_panel').hide()
        $('#store_sell').click(() =>
            $('#store_sell_panel').show()
            $('#store_buy_panel').hide()
            $('#store_sell').css({'border-bottom': '3px solid #33b5e5'})
            $('#store_buy').css({'border-bottom': '0px'})
            @storepage = "sell"
        )
        $('#store_buy').click(() =>
            $('#store_sell_panel').hide()
            $('#store_buy_panel').show()
            $('#store_buy').css({'border-bottom': '3px solid #33b5e5'})
            $('#store_sell').css({'border-bottom': '0px'})
            @storepage = "buy"
        )
        $('.modal_close').click(window.modalHide)
        $('#store_cancel').click(window.modalHide)
        $('#store_ok').click((e) =>
            if @storepage == "sell"
                obj = []
                $('#store_sell_panel .store_input input').each(() ->
                    num = parseInt($(this).val())
                    obj.push({id: $(this).attr('id'), num: num}) if num
                )
                $.put("store/#{@model.id}/sell",JSON.stringify(obj),$.noop,'json')
            else
                obj = []
                $('#store_buy_panel .store_input input').each(() ->
                    num = parseInt($(this).val())
                    obj.push({id: $(this).attr('id'), num: num}) if num
                )
                $.put("store/#{@model.id}/buy",JSON.stringify(obj),$.noop,'json')
            window.modalHide(e)
        )
        $('.store_item').each(() ->
            $(this).qtip({
                content: $('.item_effect',this).text(),
                position: {
                    my: "right top",
                    at: "left center"
                }
            })
        )
    clear : () ->
        @marker.setMap(null)
        gmaps.event.clearInstanceListeners(@marker)
        @marker = null
})
window.Store = Backbone.Model.extend({
    getLatLng : () ->
        return new gmaps.LatLng(parseFloat(@get('latitude')), parseFloat(@get('longitude')))
})
window.StoreList = Backbone.Collection.extend({
    model: Store
    update: () ->
        $.get('store', (ret) =>
            for id of window.storecontrollerlist
                window.storecontrollerlist[id].clear()
                delete window.storecontrollerlist[id]
            for item in ret
                id = item['id']
                store = new Store(item)
                @add(store)
                window.storecontrollerlist[id] = new StoreController({
                    model: store
                })
        )
})
