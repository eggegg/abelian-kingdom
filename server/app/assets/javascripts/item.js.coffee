# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
window.ItemController = Backbone.View.extend({
    tagName : "div"
    className : "item"
    initialize : () ->
        $(@el).attr("id","item#{@model.id}").appendTo('div#inventory_panel')
        $(@el).qtip({
            content: @model.get('effect')
            position: {
                my: "right top",
                at: "left center"
            }
        })
        @render()
    template : JST["backbone/templates/item"]
    render : () ->
        $(@el).html(@template(@model.toJSON()))
    clear : () ->
        @remove()
})
window.Item = Backbone.Model.extend({
})
window.ItemList = Backbone.Collection.extend({
    model: Item
    update: () ->
        $.get('item', (ret) =>
            for id of window.itemcontrollerlist
                window.itemcontrollerlist[id].clear()
                delete window.itemcontrollerlist[id]
            for it in ret
                id = it['id']
                item = new Item(it)
                @add(item)
                window.itemcontrollerlist[id] = new ItemController({
                    model: item
                })
        )
})
